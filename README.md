# partscleaner

Small script that removes the .parts files that QBitTorrent (and other torrent clients?) leaves in the download folder.

One way to use it is to make the script executable and save it somewhere in your systems $PATH and then call it from your ".bashrc" file. (just make a new line and write "partscleaner" at the end of your ".bashrc" file and save) 
That way you automatically clean you downloads folder every time you open a terminal.

WARNING: the script removes (put in trash) every file that ends with ".parts" from your ~/Downloads folder. Use carefully.

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)